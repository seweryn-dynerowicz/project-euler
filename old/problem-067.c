#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

#include "problem-067.h" // triangle[]

/* By starting at the top of the triangle below and moving to adjacent numbers on the row below,
the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route.
However, Problem 67, is the same challenge with a triangle containing one-hundred rows;
it cannot be solved by brute force, and requires a clever method! ;o)
*/

#define ROW_COUNT 100

unsigned long sums[sizeof(triangle) / sizeof(unsigned long)];

unsigned int indices[ROW_COUNT + 1];

void printTriangle(unsigned long triangle[]) {
  for(unsigned int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++) {
    if(rowIndex < 10)
      printf("Row  %ld :", rowIndex);
    else
      printf("Row %ld :", rowIndex);

    for(unsigned int index = indices[rowIndex]; index < indices[rowIndex+1]; index++) {
      if(triangle[index] < 10)
	printf("  %ld", triangle[index]);
      else
	printf(" %ld", triangle[index]);
    }
    printf("\n");
  }
}

int main(int argc, char* argv[]) {
  // Initialize indices
  for(unsigned int rowIndex = 0; rowIndex < ROW_COUNT; rowIndex++) {
    indices[rowIndex+1] = indices[rowIndex] + rowIndex + 1;
  }

  // Initialize sums triangle
  for(unsigned int rowIndex = 0 ; rowIndex < ROW_COUNT ; rowIndex++ ) {
    for(unsigned int index = indices[rowIndex]; index < indices[rowIndex+1]; index++) {
      sums[index] = triangle[index];
    }
  }

  // Proceed all the way to the summit.
  unsigned int currentRowIndex, currentRowSize, beneathRowIndex;
  unsigned long current, beneathL, beneathR;
  for(unsigned int rowIndex = ROW_COUNT - 2; 0 <= rowIndex && rowIndex <= ROW_COUNT; rowIndex--) {
    currentRowIndex = indices[rowIndex];
    beneathRowIndex = indices[rowIndex+1];
    currentRowSize  = beneathRowIndex - currentRowIndex;
    for(unsigned int index = 0; index < currentRowSize; index++) {
      beneathL = sums[beneathRowIndex + index];
      beneathR = sums[beneathRowIndex + index + 1];
      current = sums[currentRowIndex + index];
      if(beneathL >= beneathR)
	sums[currentRowIndex + index] += beneathL;
      else
	sums[currentRowIndex + index] += beneathR;
    }
  }

  printf("Triangle :\n");
  printTriangle(triangle);
  printf("\n");

  printf("Sums :\n");
  printTriangle(sums);
  printf("\n");

  printf("Maximal sum from the summit : %ld\n", sums[0]);
  
  return EXIT_SUCCESS;
}
