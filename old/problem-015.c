#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

#define ROW_COUNT 20
#define COL_COUNT 20

unsigned long latticeCounts[ROW_COUNT+1][COL_COUNT+1];

int main(int argc, char* argv[]) {
  latticeCounts[ROW_COUNT][COL_COUNT] = 1;
  unsigned long count;

  for(unsigned int row = ROW_COUNT; 0 <= row && row <= ROW_COUNT; row--) {
    for(unsigned int col = COL_COUNT; 0 <= col && col <= COL_COUNT; col--) {
      if(row < ROW_COUNT || col < COL_COUNT) {
	count = 0;

	if (row < ROW_COUNT)
	  count += latticeCounts[row+1][col];

	if (col < COL_COUNT)
	  count += latticeCounts[row][col+1];

	latticeCounts[row][col] = count;
      }
    }
  } 

  printf("Number of lattice paths from (0,0) to (%ld, %ld) : %ld\n", ROW_COUNT, COL_COUNT, latticeCounts[0][0]);
  
  return EXIT_SUCCESS;
}
