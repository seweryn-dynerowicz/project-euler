#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

unsigned long hint = 13195;
unsigned long target = 600851475143;
unsigned long maxFactor = 775147;    // sqrt(target)

int main(int argc, char* argv[]) {
  for(unsigned long factor = maxFactor; factor >= 2; factor--) {
    unsigned long divided = target / factor;
    if(divided * factor == target) {
      unsigned long isPrime = 1;
      for(unsigned long nested = factor-1; nested >= 2; nested--) {
        if(nested * (factor / nested) == factor) {
	  isPrime = 0;
	  break;
	}
      }
      if(isPrime == 1) {
	printf("Largest prime factor found : %ld\n", factor);
	break;
      }
    }
  }
  
  return EXIT_SUCCESS;
}
