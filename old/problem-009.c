#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

/* A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc. */

unsigned long squares[1000];

int main(int argc, char* argv[]) {
  for(unsigned int index = 0; index < 1000; index++) {
    squares[index] = index*index;
  }

  unsigned long cs, bs, as, product;
  unsigned int a;
  for(unsigned int c = 1; c < 1000; c++) {
    cs = squares[c];
    for(unsigned int b = 1; b < c; b++) {
      bs = squares[b];
      as = cs - bs;
      a = sqrt(as);
      if (a * a == as && a+b+c == 1000) {
	product = a*b*c;
	printf("Candidate : %ld + %ld = %ld | %d + %d + %d = 1000 => %ld\n", as, bs, cs, a, b, c, product);
      }
    }
  }

  return EXIT_SUCCESS;
}
