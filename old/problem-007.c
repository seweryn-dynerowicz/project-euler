#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

#define TRUE  1
#define FALSE 0

unsigned long primeCount = 0;
unsigned long primes[10001];


int isPrime(unsigned long candidate) {
  unsigned long squareRoot = sqrt(candidate);
  unsigned int index = 0;
  while(index < primeCount && primes[index] <= squareRoot) {
    unsigned long divisor = primes[index];
    if(divisor * (candidate / divisor) == candidate)
      return FALSE;
    index++;
  }
  return TRUE;
}

int main(int argc, char* argv[]) {
  for(unsigned long current = 2; primeCount < 10001; current++) {
    if(isPrime(current) == TRUE) {
      //printf("Prime found [%ld] ! %ld\n", primeCount, current);
      primes[primeCount] = current;
      primeCount++;
    }
  }

  printf("Prime found [%ldth] : %ld\n", primeCount, primes[primeCount-1]);
  
  return EXIT_SUCCESS;
}
