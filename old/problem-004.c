#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS
#include <string.h> // strlen()

#define TRUE  1
#define FALSE 0

#define MAX 999
#define MIN 100

int isPalindrome(char* string, int length) {
  for(unsigned int index = 0; index < length; index++) {
    if(string[index] != string[length - index - 1])
      return FALSE;
    index++;
  }
  return TRUE;
}

unsigned char number[6];
unsigned long maxPalindrome = 0;
unsigned long maxTD1 = 0;
unsigned long maxTD2 = 0;

int main(int argc, char* argv[]) {
  for(unsigned long td1 = MAX; td1 >= MIN; td1--) {
    for(unsigned long td2 = MAX; td2 >= MIN; td2--) {
      unsigned long product = td1 * td2;
      unsigned long length = 6;
      if(product >= 100000) {
        sprintf(number, "%ld", product);
        if(number[0] == number[5] && number[1] == number[4] && number[2] == number[3]) {
	  if(maxPalindrome < product) {
	    maxPalindrome = product;
	    maxTD1 = td1;
	    maxTD2 = td2;
	  }
        }
      }
    }
  }

  printf("Largest palindrome found : %ld = %ld x %ld\n",maxPalindrome, maxTD1, maxTD2);
  return EXIT_SUCCESS;
}
