#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

#define TRUE  1
#define FALSE 0

#define COUNT 148933

unsigned long primeCount = 0;
unsigned long primes[COUNT];

int isPrime(unsigned long candidate) {
  unsigned long squareRoot = sqrt(candidate);
  unsigned int index = 0;
  while(index < primeCount && primes[index] <= squareRoot) {
    unsigned long divisor = primes[index];
    if(divisor * (candidate / divisor) == candidate)
      return FALSE;
    index++;
  }
  return TRUE;
}

int main(int argc, char* argv[]) {
  unsigned long total = 2;
  primes[0] = 2; primeCount = 1;
  for(unsigned long current = 3; primeCount < COUNT; current += 2) {
    if(isPrime(current) == TRUE) {
      //printf("Prime found [%ld] ! %ld\n", primeCount, current);
      primes[primeCount] = current;
      total += current;
      primeCount++;
    }
  }

  printf("Sum of primes smaller than 2.000.000 : %ld\n", total);
  
  return EXIT_SUCCESS;
}
