#include <math.h>   // sqrt()
#include <stdio.h>  // printf()
#include <stdlib.h> // EXIT_SUCCESS

/* The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms.
Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*/

// Go up to 1.000.000 as the starting number for the Collatz chain.
#define LIMIT 1000000

// Number of starts whose chain length will be stored.
// Fiddle with SIZE based on your available memory.
#define SIZE 1000000
unsigned long collatzLength[SIZE];

unsigned long collatz(unsigned long number) {
  if(number % 2 == 1) // Odd number
    return 3*number + 1;
  else // Even number
    return number / 2;
}

unsigned long chainLength(unsigned int start) {
  if(start == 1)
    return 0;
  else {
    if(start < SIZE) {
      if(collatzLength[start] == 0)
	collatzLength[start] = 1 + chainLength(collatz(start));
      return collatzLength[start];
    } else {
      return 1 + chainLength(collatz(start));
    }
  }
}

int main(int argc, char* argv[]) {
  unsigned long maxChainStart = 1, maxChainLength = 1, length;
  for(unsigned long start = 2; start < LIMIT; start++) {
    length = chainLength(start);
    if(maxChainLength < length) {
      maxChainStart = start;
      maxChainLength = length;
    }
  }
  printf("Longest Collatz chain : %ld [start=%ld]\n", maxChainLength, maxChainStart);
  
  return EXIT_SUCCESS;
}
