module Problem016 where

import Utilities

problem016 :: IO()
problem016 = do (print . sum . map decode . show) (2^1000)
