module Utilities where

import Data.Char

decode :: Char -> Int
decode digit
  | '0' <= digit && digit <= '9' = ord(digit) - ord('0')
  | otherwise = error "Not a valid digit."
