module Problem020 where

import Utilities
import Math.Combinatorics.Exact.Factorial (factorial)

problem020 :: IO()
problem020 = do (print . sum . map decode . show) (factorial 100 :: Integer)
